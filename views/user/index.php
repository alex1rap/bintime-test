<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'username',
                'label' => 'Username',
                'value' => function ($model) {
                    return Html::a($model->username, ['user/view', 'id' => $model->id], ['data-pjax' => '0']);
                },
                'format' => 'raw'
            ],
            'password',
            'first_name',
            'last_name',
            //'sex',
            //'made_at',
            //'email:email',

            ['class' => 'app\widgets\AddressActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
