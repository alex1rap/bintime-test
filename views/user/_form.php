<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $model_ app\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->dropDownList([ 'UNKNOWN' => 'UNKNOWN', 'MALE' => 'MALE', 'FEMALE' => 'FEMALE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'made_at')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


    <?php if (!empty($model_)) :?>
    <?= $form->field($model_, 'post_index')->textInput() ?>

    <?= $form->field($model_, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_, 'street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_, 'house')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_, 'office')->textInput() ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
