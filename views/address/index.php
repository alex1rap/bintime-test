<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchAddress */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Address';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user.username',
                'label' => 'Username',
                'value' => function ($model) {
                    return Html::a($model->user->username, ['user/view', 'id' => $model->user_id], ['data-pjax' => '0']);
                },
                'format' => 'raw'
            ],
            'post_index',
            'country',
            'city',
            'street',
            'house',
            'office',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
