<?php

namespace app\widgets;

use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

class AddressActionColumn extends ActionColumn
{
    private static $controllerName = 'address';

    public function createUrl($action, $model, $key, $index)
    {
        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index, $this);
        }

        $params = is_array($key) ? $key : ['id' => (string) $key];
        $params[0] = self::$controllerName ? self::$controllerName . '/' . $action : $action;

        return Url::toRoute($params);
    }
}
