<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Address;

/**
 * SearchAddress represents the model behind the search form of `app\models\Address`.
 */
class SearchAddress extends Address
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'post_index', 'office', 'user_id'], 'integer'],
            [['user.username'], 'safe'],
            [['country', 'city', 'street', 'house'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Address::find();
        //var_dump($query, $params);exit;
        if (isset($params['SearchAddress']['user'])) {
            $user = Users::findOne(['username' => $params['SearchAddress']['user']]);
            if ($user !== null) {
                $params['SearchAddress']['user_id'] = $user->id;
            }
            unset($user, $params['SearchAddress']['user']);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 5
            ]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['user' => function ($query) {
            $query->from(['user' => 'users']);
        }]);
        $dataProvider->sort->attributes['user.username'] = [
            'asc' => ['user.username' => SORT_ASC],
            'desc' => ['user.username' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'post_index' => $this->post_index,
            'office' => $this->office,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['LIKE', 'user.username', $this->getAttribute('user.username')]);

        return $dataProvider;
    }
}
