<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property int $id
 * @property int $post_index
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house
 * @property int $office
 * @property int $user_id
 *
 * @property Users $user
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_index', 'country', 'city', 'street', 'house', 'user_id'], 'required'],
            [['post_index', 'office', 'user_id'], 'integer'],
            [['country'], 'string', 'max' => 2],
            ['country', 'validateCountry'],
            [['city', 'street'], 'string', 'max' => 32],
            [['house'], 'string', 'max' => 4],
            ['house', 'validateHouse'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function validateCountry()
    {
        if (!preg_match("/^[A-Z]{2}$/", $this->country)) {
            $this->addError('country', 'Country field must have two letters in upper case.');
        }
    }

    public function validateHouse()
    {
        if (!preg_match("/^\d+([a-zа-яєїё])?$/i", $this->house)) {
            $this->addError('house', 'House number must have only digits or digits and one letter');
        }
    }

    public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['user.username']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_index' => 'Post Index',
            'country' => 'Country',
            'city' => 'City',
            'street' => 'Street',
            'house' => 'House',
            'office' => 'Office',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
